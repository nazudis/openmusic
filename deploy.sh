#!/bin/bash
cd /home/ubuntu/openmusic/
git checkout .
git checkout master
git pull origin master

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

npm i --only=prod
npm run migrate up

pm2 restart pm2.json