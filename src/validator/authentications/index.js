const {
  PostAuthenticationPayloadSchema,
  PutAuthenticationPayloadSchema,
  DeleteAuthenticationPayloadSchema,
} = require('./schema')
const InvariantError = require('../../exceptions/InvariantError')

const AuthenticationsValidator = {
  validate: (schema, payload) => {
    const validationResult = schema.validate(payload)
    if (validationResult.error) {
      throw new InvariantError(validationResult.error.message)
    }

    return validationResult.value
  },

  validatePostAuthenticationPayload: (payload) =>
    AuthenticationsValidator.validate(PostAuthenticationPayloadSchema, payload),

  validatePutAuthenticationPayload: (payload) =>
    AuthenticationsValidator.validate(PutAuthenticationPayloadSchema, payload),

  validateDeleteAuthenticationPayload: (payload) =>
    AuthenticationsValidator.validate(
      DeleteAuthenticationPayloadSchema,
      payload
    ),
}

module.exports = AuthenticationsValidator
