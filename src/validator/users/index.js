const { InvariantError } = require('../../exceptions')
const { UserParamsSchema, UserPayloadSchema } = require('./schema')

const UsersValidator = {
  validate: (schema, payload) => {
    const validationResult = schema.validate(payload)
    if (validationResult.error) {
      throw new InvariantError(validationResult.error.message)
    }

    return validationResult.value
  },
  validateUserPayload: (payload) =>
    UsersValidator.validate(UserPayloadSchema, payload),

  validateUserParams: (params) =>
    UsersValidator.validate(UserParamsSchema, params),
}

module.exports = UsersValidator
