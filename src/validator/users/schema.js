const Joi = require('joi')

const UserPayloadSchema = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().required(),
  fullname: Joi.string().required(),
})

const UserParamsSchema = Joi.object({
  userId: Joi.string().required(),
})

module.exports = { UserPayloadSchema, UserParamsSchema }
