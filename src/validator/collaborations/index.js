const { InvariantError } = require('../../exceptions')
const { CollaborationsPayloadSchema } = require('./schema')

const CollaborationsValidator = {
  validate(schema, payload) {
    const { error, value } = schema.validate(payload)
    if (error) {
      throw InvariantError(error.message)
    }

    return value
  },
  validateCollaborationsPayload: (payload) =>
    CollaborationsValidator.validate(CollaborationsPayloadSchema, payload),
}

module.exports = CollaborationsValidator
