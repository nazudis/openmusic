const Joi = require('joi')

const PlaylistPayloadSchema = Joi.object({
  name: Joi.string().required(),
})

const PlaylistParamsSchema = Joi.object({
  playlistId: Joi.string().required(),
})

const PlaylistSongPayloadSchema = Joi.object({
  songId: Joi.string().required(),
})

module.exports = {
  PlaylistParamsSchema,
  PlaylistPayloadSchema,
  PlaylistSongPayloadSchema,
}
