const { InvariantError } = require('../../exceptions')
const {
  PlaylistPayloadSchema,
  PlaylistParamsSchema,
  PlaylistSongPayloadSchema,
} = require('./schema')

const PlaylistsValidator = {
  validate(schema, payload) {
    const { error, value } = schema.validate(payload)
    if (error) {
      throw new InvariantError(error.message)
    }

    return value
  },
  validatePlaylistPayload: (payload) =>
    PlaylistsValidator.validate(PlaylistPayloadSchema, payload),
  validatePlaylistParams: (params) =>
    PlaylistsValidator.validate(PlaylistParamsSchema, params),
  validatePlaylistSongPayload: (payload) =>
    PlaylistsValidator.validate(PlaylistSongPayloadSchema, payload),
}

module.exports = PlaylistsValidator
