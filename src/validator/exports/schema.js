const Joi = require('joi')

const ExportPlaylistsPayloadSchema = Joi.object({
  targetEmail: Joi.string().email().lowercase().required(),
})

const ExportPlaylistsParamsSchema = Joi.object({
  playlistId: Joi.string().required(),
})

module.exports = { ExportPlaylistsPayloadSchema, ExportPlaylistsParamsSchema }
