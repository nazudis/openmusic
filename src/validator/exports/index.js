const {
  ExportPlaylistsPayloadSchema,
  ExportPlaylistsParamsSchema,
} = require('./schema')
const { InvariantError } = require('../../exceptions')

const ExportsValidator = {
  validate: (schema, payload) => {
    const { error, value } = schema.validate(payload)
    if (error) {
      throw new InvariantError(error.message)
    }

    return value
  },

  validateExportPlaylistsPayload: (payload) =>
    ExportsValidator.validate(ExportPlaylistsPayloadSchema, payload),

  validateExportPlaylistsParams: (params) =>
    ExportsValidator.validate(ExportPlaylistsParamsSchema, params),
}

module.exports = ExportsValidator
