const { ImageHeadersSchema } = require('./schema')
const { InvariantError } = require('../../exceptions')

const UploadsValidator = {
  validateImageHeaders: (headers) => {
    const { error } = ImageHeadersSchema.validate(headers)

    if (error) {
      throw new InvariantError(error.message)
    }
  },
}

module.exports = UploadsValidator
