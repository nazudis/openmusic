const Joi = require('joi')

const year = new Date().getFullYear()

const SongPayloadSchema = Joi.object({
  title: Joi.string().required(),
  year: Joi.number().min(1800).max(year).required(),
  performer: Joi.string().required(),
  genre: Joi.string().optional(),
  duration: Joi.number().min(60).optional(),
})

const SongsQuerySchema = Joi.object({
  searchTitle: Joi.string().allow('').optional().default(''),
})

const SongParamsSchema = Joi.object({
  songId: Joi.string().required(),
})

module.exports = { SongPayloadSchema, SongsQuerySchema, SongParamsSchema }
