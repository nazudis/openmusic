const { InvariantError } = require('../../exceptions')
const {
  SongPayloadSchema,
  SongsQuerySchema,
  SongParamsSchema,
} = require('./schema')

const SongsValidator = {
  validate: (schema, payload) => {
    const validationResult = schema.validate(payload)
    if (validationResult.error) {
      throw new InvariantError(validationResult.error.message)
    }

    return validationResult.value
  },
  validateSongPayload: (payload) =>
    SongsValidator.validate(SongPayloadSchema, payload),

  validateSongsQuery: (query) =>
    SongsValidator.validate(SongsQuerySchema, query),

  validateSongParams: (params) =>
    SongsValidator.validate(SongParamsSchema, params),
}

module.exports = SongsValidator
