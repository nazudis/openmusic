const Jwt = require('@hapi/jwt')

const SongsService = require('./services/postgres/SongsService')
const SongsValidator = require('./validator/songs')
const songs = require('./api/songs')

const UsersService = require('./services/postgres/UsersService')
const UsersValidator = require('./validator/users')
const users = require('./api/users')

const AuthenticationsService = require('./services/postgres/AuthenticationsService')
const AuthenticationsValidator = require('./validator/authentications')
const authentications = require('./api/authentications')

const TokenManager = require('./tokenize/TokenManager')

const PlaylistsService = require('./services/postgres/PlaylistsService')
const PlaylistSongsService = require('./services/postgres/PlaylistSongsService')
const PlaylistsValidator = require('./validator/playlists')
const playlists = require('./api/playlists')

const CollaborationsService = require('./services/postgres/CollaborationsService')
const CollaborationsValidator = require('./validator/collaborations')
const collaborations = require('./api/collaborations')

const ProducerService = require('./services/rabbitmq/ProducerService')
const _exports = require('./api/exports')
const ExportsValidator = require('./validator/exports')

const StorageService = require('./services/storage/StorageService')
const UploadsValidator = require('./validator/uploads')
const uploads = require('./api/uploads')

const CacheService = require('./services/redis/CacheService')

async function Plugins(server) {
  // Register JWT and Define Strategy
  await server.register(Jwt)
  server.auth.strategy('openmusic_jwt', 'jwt', {
    keys: process.env.ACCESS_TOKEN_KEY,
    verify: {
      aud: false,
      iss: false,
      sub: false,
      maxAgeSec: process.env.ACCESS_TOKEN_AGE,
    },
    validate: (artifacts) => ({
      isValid: true,
      credentials: {
        id: artifacts.decoded.payload.id,
      },
    }),
  })

  // Define Services
  const songsService = new SongsService()
  const usersService = new UsersService()
  const authenticationsService = new AuthenticationsService()
  const collaborationsService = new CollaborationsService()
  const playlistsService = new PlaylistsService(collaborationsService)
  const cacheService = new CacheService()
  const playlistSongsService = new PlaylistSongsService(
    songsService,
    cacheService
  )
  const storageService = new StorageService()

  await server.register([
    {
      plugin: songs,
      options: {
        service: songsService,
        validator: SongsValidator,
      },
    },
    {
      plugin: users,
      options: {
        service: usersService,
        validator: UsersValidator,
      },
    },
    {
      plugin: authentications,
      options: {
        authenticationsService,
        usersService,
        tokenManager: TokenManager,
        validator: AuthenticationsValidator,
      },
    },
    {
      plugin: playlists,
      options: {
        service: { playlistsService, playlistSongsService },
        validator: PlaylistsValidator,
      },
    },
    {
      plugin: collaborations,
      options: {
        service: { collaborationsService, playlistsService },
        validator: CollaborationsValidator,
      },
    },
    {
      plugin: _exports,
      options: {
        service: { ProducerService, playlistsService },
        validator: ExportsValidator,
      },
    },
    {
      plugin: uploads,
      options: {
        service: storageService,
        validator: UploadsValidator,
      },
    },
  ])
}

module.exports = Plugins
