const { ClientError } = require('../exceptions')

exports.HapiResponse = function HapiResponse(
  hapi,
  message = '',
  code = 200,
  status = 'success',
  data
) {
  const response = hapi.response({
    status,
    message,
    data,
  })
  response.code(code)

  return response
}

exports.ValidateResponse = (request, h) => {
  const { response } = request

  if (response instanceof ClientError) {
    const newResponse = h.response({
      status: 'fail',
      message: response.message,
    })
    newResponse.code(response.statusCode)
    return newResponse
  }

  // Show log if Server Error
  if (response.isServer) {
    console.error(response)
  }

  return response.continue || response
}
