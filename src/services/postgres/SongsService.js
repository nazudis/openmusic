const { nanoid } = require('nanoid')
const { Pool } = require('pg')
const { NotFoundError, InvariantError } = require('../../exceptions')

class SongsService {
  constructor() {
    this._pool = new Pool()
  }

  async addSong(payload) {
    const id = `song-${nanoid(16)}`
    const insertedAt = new Date().toISOString()

    const query = {
      text: 'INSERT INTO songs VALUES($1, $2, $3, $4, $5, $6, $7, $7) RETURNING id',
      values: [id, ...Object.values(payload), insertedAt],
    }
    const result = await this._pool.query(query)

    if (!result.rows[0].id) {
      throw new InvariantError('Failed to add song.')
    }

    return result.rows[0].id
  }

  async getSongs({ searchTitle }) {
    const query = {
      text: 'SELECT id, title, performer FROM songs WHERE id ILIKE $1',
      values: [`%${searchTitle}%`],
    }

    const result = await this._pool.query(query)

    return result.rows
  }

  async getSongById(id) {
    const query = {
      text: 'SELECT * FROM songs WHERE id=$1',
      values: [id],
    }

    const result = await this._pool.query(query)
    if (!result.rows[0]) {
      throw new NotFoundError(`Song with ID ${id} not found`)
    }

    return result.rows[0]
  }

  async updateSongById(id, { title, year, performer, genre, duration }) {
    const now = new Date().toISOString()

    const query = {
      text: 'UPDATE songs SET title=$1, year=$2, performer=$3, genre=COALESCE($4, genre), duration=COALESCE($5,duration), "updated_at"=$6 WHERE id=$7',
      values: [title, year, performer, genre, duration, now, id],
    }

    const result = await this._pool.query(query)
    if (!result.rowCount) {
      throw new NotFoundError('Failed to update. Song ID not found')
    }
  }

  async deleteSongById(id) {
    const query = {
      text: 'DELETE FROM songs WHERE id=$1',
      values: [id],
    }

    const result = await this._pool.query(query)
    if (!result.rowCount) {
      throw new NotFoundError('Failed to delete. Song ID not found')
    }
  }

  async verifySongId(id) {
    try {
      await this.getSongById(id)
    } catch (err) {
      if (err instanceof NotFoundError) {
        throw new InvariantError('Invalid song Id.')
      }
      throw err
    }
  }
}

module.exports = SongsService
