const { nanoid } = require('nanoid')
const { Pool } = require('pg')
const { InvariantError, NotFoundError } = require('../../exceptions')

class PlaylistSongsService {
  constructor(songService, cacheService) {
    this._pool = new Pool()
    this._songService = songService
    this._cacheService = cacheService
  }

  async addPlaylistSong({ playlistId, songId }) {
    const id = `playlistsong-${nanoid(16)}`
    const insertedAt = new Date().toISOString()

    const query = {
      text: 'INSERT INTO playlistsongs VALUES($1, $2, $3, $4, $4)',
      values: [id, playlistId, songId, insertedAt],
    }
    const result = await this._pool.query(query)
    if (!result.rowCount) {
      throw new InvariantError('Failed add song to playlist.')
    }

    await this._cacheService.delete(`playlistsongs:${playlistId}`)
  }

  async getPlaylistSongs(playlistId) {
    try {
      // try to get from cache
      const result = await this._cacheService.get(`playlistsongs:${playlistId}`)
      return JSON.parse(result)
    } catch (error) {
      // cache not found. continue to get from db
      const query = {
        text: `SELECT playlistsongs.song_id AS id, songs.title, songs.performer FROM playlistsongs
        JOIN songs ON songs.id = playlistsongs.song_id
        WHERE playlistsongs.playlist_id = $1
        GROUP BY playlistsongs.song_id, playlistsongs.id, songs.id
        `,
        values: [playlistId],
      }

      const result = await this._pool.query(query)

      await this._cacheService.set(
        `playlistsongs:${playlistId}`,
        JSON.stringify(result.rows)
      )

      return result.rows
    }
  }

  async deletePlaylistSongById(playlistId, songId) {
    const query = {
      text: 'DELETE FROM playlistsongs WHERE playlist_id = $1 AND song_id = $2',
      values: [playlistId, songId],
    }

    await this._songService.verifySongId(songId)

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new NotFoundError(
        `Failed to delete song. The song does'nt exists in the playlists`
      )
    }

    await this._cacheService.delete(`playlistsongs:${playlistId}`)
  }
}

module.exports = PlaylistSongsService
