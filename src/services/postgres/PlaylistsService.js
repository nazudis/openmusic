const { nanoid } = require('nanoid')
const { Pool } = require('pg')
const {
  InvariantError,
  AuthorizationError,
  NotFoundError,
} = require('../../exceptions')

class PlaylistsService {
  constructor(collaborationsService) {
    this._pool = new Pool()
    this._collaborationService = collaborationsService
  }

  async addPlaylist({ ownerId, name }) {
    const id = `playlist-${nanoid(16)}`
    const insertedAt = new Date().toISOString()

    const query = {
      text: 'INSERT INTO playlists VALUES($1, $2, $3, $4, $4) RETURNING id',
      values: [id, name, ownerId, insertedAt],
    }

    const result = await this._pool.query(query)

    if (!result.rows[0].id) {
      throw new InvariantError('Failed to add playlist.')
    }

    return result.rows[0].id
  }

  async getPlaylists(credentialId) {
    const query = {
      text: `SELECT playlists.id, playlists.name, users.username FROM playlists
      JOIN users ON users.id = playlists.owner
      LEFT JOIN collaborations ON collaborations.playlist_id = playlists.id
      WHERE playlists.owner = $1 OR collaborations.user_id = $1
      GROUP BY playlists.id, users.username`,
      values: [credentialId],
    }
    const result = await this._pool.query(query)

    return result.rows
  }

  async verifyPlaylistOwner(playlistId, owner) {
    const query = {
      text: 'SELECT playlists.owner FROM playlists WHERE id = $1',
      values: [playlistId],
    }
    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new NotFoundError('Playlist not found.')
    }

    const playlists = result.rows[0]
    if (playlists.owner !== owner) {
      throw new AuthorizationError('Youre not eligible to acces this.')
    }
  }

  async deletePlaylistById(playlistId) {
    const query = {
      text: 'DELETE FROM playlists WHERE id = $1',
      values: [playlistId],
    }
    const result = await this._pool.query(query)
    if (!result.rowCount) {
      throw new NotFoundError('Failed delete playlist. Playlist not found.')
    }
  }

  async verifyPlaylistAccess(playlistId, userId) {
    try {
      await this.verifyPlaylistOwner(playlistId, userId)
    } catch (error) {
      if (error instanceof NotFoundError) {
        throw error
      }
      try {
        await this._collaborationService.verifyCollaborator(playlistId, userId)
      } catch {
        throw error
      }
    }
  }
}

module.exports = PlaylistsService
