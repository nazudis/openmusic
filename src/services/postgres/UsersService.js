const { hash, compare } = require('bcrypt')
const { nanoid } = require('nanoid')
const { Pool } = require('pg')
const {
  NotFoundError,
  InvariantError,
  AuthenticationError,
} = require('../../exceptions')

class UsersService {
  constructor() {
    this._pool = new Pool()
  }

  async addUser({ username, password, fullname }) {
    await this.verifyNewUsername(username)

    const id = `user-${nanoid(16)}`
    const insertedAt = new Date().toISOString()
    const hashPassword = await hash(password, 10)

    const query = {
      text: 'INSERT INTO users VALUES($1, $2, $3, $4, $5,$5) RETURNING id',
      values: [id, username, hashPassword, fullname, insertedAt],
    }
    const result = await this._pool.query(query)

    if (!result.rows[0].id) {
      throw new InvariantError('Failed to add user')
    }

    return result.rows[0].id
  }

  async verifyNewUsername(username) {
    const query = {
      text: 'SELECT username FROM users WHERE username = $1',
      values: [username],
    }

    const result = await this._pool.query(query)

    if (result.rowCount) {
      throw new InvariantError('Failed add user. Username has been taken.')
    }
  }

  async getUserById(id) {
    const query = {
      text: 'SELECT id, username, fullname FROM users WHERE id=$1',
      values: [id],
    }

    const result = await this._pool.query(query)
    if (!result.rows[0]) {
      throw new NotFoundError(`User with ID ${id} not found`)
    }

    return result.rows[0]
  }

  async verifyUserCredential(username, password) {
    const query = {
      text: 'SELECT id, password FROM users WHERE username = $1',
      values: [username],
    }

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new AuthenticationError('Invalid credential username.')
    }

    const { id, password: hashedPassword } = result.rows[0]

    const match = await compare(password, hashedPassword)

    if (!match) {
      throw new AuthenticationError('Invalid password.')
    }

    return id
  }
}

module.exports = UsersService
