exports.mapToObject = (obj) => {
  const newObj = { ...obj }
  Object.keys(obj).forEach((k) => {
    if (k.includes('_')) {
      const char = k.split('_')
      let newProps = ''
      char.forEach((c, i) => {
        let newSubChar = c
        if (i > 0) {
          newSubChar = `${c.charAt(0).toUpperCase()}${c.slice(1)}`
        }
        newProps += newSubChar
      })

      delete newObj[k]
      newObj[newProps] = obj[k]
    }
  })

  return newObj
}
