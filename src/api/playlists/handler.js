const { HapiResponse } = require('../../response')

class PlaylistsHandler {
  constructor(service, validator) {
    this._playlistsService = service.playlistsService
    this._playlistSongsService = service.playlistSongsService
    this._validator = validator

    this.postPlaylistHandler = this.postPlaylistHandler.bind(this)
    this.getPlaylistsHandler = this.getPlaylistsHandler.bind(this)
    this.deletePlaylistHandler = this.deletePlaylistHandler.bind(this)
    this.postPlaylistSongHandler = this.postPlaylistSongHandler.bind(this)
    this.getPlaylistSongHandler = this.getPlaylistSongHandler.bind(this)
    this.deletePlaylistSongHandler = this.deletePlaylistSongHandler.bind(this)
  }

  async postPlaylistHandler({ payload, auth }, h) {
    const { name } = this._validator.validatePlaylistPayload(payload)
    const { id: ownerId } = auth.credentials

    const playlistId = await this._playlistsService.addPlaylist({
      ownerId,
      name,
    })

    return HapiResponse(h, 'Success add playlist', 201, 'success', {
      playlistId,
    })
  }

  async getPlaylistsHandler({ auth }, h) {
    const { id: credentialId } = auth.credentials

    const playlists = await this._playlistsService.getPlaylists(credentialId)

    return HapiResponse(h, 'Success get playlists.', 200, 'success', {
      playlists,
    })
  }

  async deletePlaylistHandler({ params, auth }, h) {
    const { playlistId } = this._validator.validatePlaylistParams(params)
    const { id: credentialId } = auth.credentials

    await this._playlistsService.verifyPlaylistOwner(playlistId, credentialId)
    await this._playlistsService.deletePlaylistById(playlistId)

    return HapiResponse(h, 'Success delete playlist', 200, 'success')
  }

  async postPlaylistSongHandler({ auth, params, payload }, h) {
    const { songId } = this._validator.validatePlaylistSongPayload(payload)
    const { playlistId } = this._validator.validatePlaylistParams(params)
    const { id: credentialId } = auth.credentials

    await this._playlistsService.verifyPlaylistAccess(playlistId, credentialId)
    await this._playlistSongsService.addPlaylistSong({ playlistId, songId })

    return HapiResponse(h, 'Success add song to playlist.', 201, 'success')
  }

  async getPlaylistSongHandler({ params, auth }, h) {
    const { playlistId } = this._validator.validatePlaylistParams(params)
    const { id: credentialId } = auth.credentials

    await this._playlistsService.verifyPlaylistAccess(playlistId, credentialId)
    const songs = await this._playlistSongsService.getPlaylistSongs(playlistId)

    return HapiResponse(h, 'Success get songs from playlist.', 200, 'success', {
      songs,
    })
  }

  async deletePlaylistSongHandler({ params, payload, auth }, h) {
    const { playlistId } = this._validator.validatePlaylistParams(params)
    const { songId } = this._validator.validatePlaylistSongPayload(payload)
    const { id: credentialId } = auth.credentials

    await this._playlistsService.verifyPlaylistAccess(playlistId, credentialId)
    await this._playlistSongsService.deletePlaylistSongById(playlistId, songId)

    return HapiResponse(
      h,
      'Success to delete song from playlist',
      200,
      'success'
    )
  }
}

module.exports = PlaylistsHandler
