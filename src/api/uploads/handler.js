const { HapiResponse } = require('../../response')

class UploadsHandler {
  constructor(service, validator) {
    this._service = service
    this._validator = validator

    this.postUploadImageHandler = this.postUploadImageHandler.bind(this)
  }

  async postUploadImageHandler({ payload: { data } }, h) {
    this._validator.validateImageHeaders(data.hapi.headers)

    const pictureUrl = await this._service.writeFile(data, data.hapi)

    return HapiResponse(h, 'Success to upload image.', 201, 'success', {
      pictureUrl,
    })
  }
}

module.exports = UploadsHandler
