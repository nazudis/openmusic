const { HapiResponse } = require('../../response')

class UsersHandler {
  constructor(service, validator) {
    this._service = service
    this._validator = validator

    this.postUserHandler = this.postUserHandler.bind(this)
    this.getUserHandler = this.getUserHandler.bind(this)
  }

  async postUserHandler({ payload }, h) {
    const value = this._validator.validateUserPayload(payload)

    const userId = await this._service.addUser(value)

    return HapiResponse(h, 'Success add user.', 201, 'success', { userId })
  }

  async getUserHandler({ params }, h) {
    const value = this._validator.validateUserParams(params)

    const user = await this._service.getUserById(value.userId)

    return HapiResponse(h, 'Success get user.', 200, 'success', { user })
  }
}

module.exports = UsersHandler
