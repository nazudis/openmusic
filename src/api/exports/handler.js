const { HapiResponse } = require('../../response')

class ExportsHandler {
  constructor(service, validator) {
    this._producerService = service.ProducerService
    this._playlistsService = service.playlistsService
    this._validator = validator

    this.postExportPlaylistsHandler = this.postExportPlaylistsHandler.bind(this)
  }

  async postExportPlaylistsHandler({ payload, params, auth }, h) {
    const { playlistId } = this._validator.validateExportPlaylistsParams(params)
    const { targetEmail } =
      this._validator.validateExportPlaylistsPayload(payload)
    const { id: credentialId } = auth.credentials

    await this._playlistsService.verifyPlaylistAccess(playlistId, credentialId)

    const message = {
      playlistId,
      userId: credentialId,
      targetEmail,
    }
    await this._producerService.sendMessage(
      'export:playlists',
      JSON.stringify(message)
    )

    return HapiResponse(h, 'Request in proccess.', 201, 'success')
  }
}

module.exports = ExportsHandler
