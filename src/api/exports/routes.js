const options = { auth: 'openmusic_jwt' }

const routes = (handler) => [
  {
    method: 'POST',
    path: '/exports/playlists/{playlistId}',
    handler: handler.postExportPlaylistsHandler,
    options,
  },
]

module.exports = routes
