const { HapiResponse } = require('../../response')

class CollaborationsHandler {
  constructor(service, validator) {
    this._playlistsService = service.playlistsService
    this._collaborationsService = service.collaborationsService
    this._validator = validator

    this.postCollaborationHandler = this.postCollaborationHandler.bind(this)
    this.deleteCollaborationHandler = this.deleteCollaborationHandler.bind(this)
  }

  async postCollaborationHandler({ payload, auth }, h) {
    const { playlistId, userId } =
      this._validator.validateCollaborationsPayload(payload)
    const { id: credentialId } = auth.credentials

    await this._playlistsService.verifyPlaylistOwner(playlistId, credentialId)
    const collaborationId = await this._collaborationsService.addCollaboration(
      playlistId,
      userId
    )

    return HapiResponse(h, 'Success add collaboration.', 201, 'success', {
      collaborationId,
    })
  }

  async deleteCollaborationHandler({ payload, auth }, h) {
    const { playlistId, userId } =
      this._validator.validateCollaborationsPayload(payload)
    const { id: credentialId } = auth.credentials

    await this._playlistsService.verifyPlaylistOwner(playlistId, credentialId)
    await this._collaborationsService.deleteCollaboration(playlistId, userId)

    return HapiResponse(h, 'Success delete collaboration.', 200, 'success')
  }
}

module.exports = CollaborationsHandler
