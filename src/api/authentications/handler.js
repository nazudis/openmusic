const { HapiResponse } = require('../../response')

class AuthenticationsHandler {
  constructor(authenticationsService, usersService, tokenManager, validator) {
    this._authenticationsService = authenticationsService
    this._usersService = usersService
    this._tokenManager = tokenManager
    this._validator = validator

    this.postAuthenticationHandler = this.postAuthenticationHandler.bind(this)
    this.putAuthenticationHandler = this.putAuthenticationHandler.bind(this)
    this.deleteAuthenticationHandler =
      this.deleteAuthenticationHandler.bind(this)
  }

  async postAuthenticationHandler({ payload }, h) {
    const value = this._validator.validatePostAuthenticationPayload(payload)

    const { username, password } = value
    const id = await this._usersService.verifyUserCredential(username, password)

    const accessToken = this._tokenManager.generateAccessToken({ id })
    const refreshToken = this._tokenManager.generateRefreshToken({ id })

    await this._authenticationsService.addRefreshToken(refreshToken)

    return HapiResponse(h, 'Authentication success to added.', 201, 'success', {
      accessToken,
      refreshToken,
    })
  }

  async putAuthenticationHandler({ payload }, h) {
    const value = this._validator.validatePutAuthenticationPayload(payload)

    const { refreshToken } = value
    await this._authenticationsService.verifyRefreshToken(value.refreshToken)
    const { id } = this._tokenManager.verifyRefreshToken(refreshToken)

    const accessToken = this._tokenManager.generateAccessToken({ id })

    return HapiResponse(h, 'Success to renew access token.', 200, 'success', {
      accessToken,
    })
  }

  async deleteAuthenticationHandler({ payload }, h) {
    const value = this._validator.validateDeleteAuthenticationPayload(payload)

    const { refreshToken } = value
    await this._authenticationsService.verifyRefreshToken(refreshToken)
    await this._authenticationsService.deleteRefreshToken(refreshToken)

    return HapiResponse(h, 'Success to delete access token.', 200)
  }
}

module.exports = AuthenticationsHandler
