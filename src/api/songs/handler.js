const { HapiResponse } = require('../../response')
const { mapToObject } = require('../../utils/mapData')

class SongsHandler {
  constructor(service, validator) {
    this._service = service
    this._validator = validator

    this.postSongHandler = this.postSongHandler.bind(this)
    this.getSongsHandler = this.getSongsHandler.bind(this)
    this.getSongHandler = this.getSongHandler.bind(this)
    this.putSongHandler = this.putSongHandler.bind(this)
    this.deleteSongHandler = this.deleteSongHandler.bind(this)
  }

  async postSongHandler(req, h) {
    const value = this._validator.validateSongPayload(req.payload)

    const songId = await this._service.addSong(value)

    return HapiResponse(h, 'Success add song.', 201, 'success', { songId })
  }

  async getSongsHandler(req, h) {
    const value = this._validator.validateSongsQuery(req.query)

    const songs = await this._service.getSongs(value)

    return HapiResponse(h, 'Success get songs', 200, 'success', { songs })
  }

  async getSongHandler(req, h) {
    const value = this._validator.validateSongParams(req.params)

    let song = await this._service.getSongById(value.songId)
    song = mapToObject(song)

    return HapiResponse(h, 'Success get song.', 200, 'success', { song })
  }

  async putSongHandler(req, h) {
    const { songId } = this._validator.validateSongParams(req.params)
    const payload = this._validator.validateSongPayload(req.payload)

    await this._service.updateSongById(songId, payload)

    return HapiResponse(h, 'Success update song.', 200, 'success')
  }

  async deleteSongHandler(req, h) {
    const { songId } = this._validator.validateSongParams(req.params)

    await this._service.deleteSongById(songId)

    return HapiResponse(h, 'Success delete song.', 200, 'success')
  }
}

module.exports = SongsHandler
