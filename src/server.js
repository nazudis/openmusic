require('dotenv/config')

const Hapi = require('@hapi/hapi')
const AppJson = require('../package.json')
const Plugins = require('./plugins')
const { ValidateResponse } = require('./response')

const init = async () => {
  const server = Hapi.server({
    port: process.env.PORT,
    host: process.env.HOST,
    routes: {
      cors: {
        origin: ['*'],
      },
    },
  })

  server.route({
    method: 'GET',
    path: '/',
    handler: () => ({
      message: 'Welcome!',
      name: AppJson.name,
      version: AppJson.version,
      description: AppJson.description,
      author: AppJson.author,
      repository: AppJson.repository,
    }),
  })

  server.ext('onPreResponse', ValidateResponse)

  await Plugins(server)

  await server.start()
  console.log(`[INFO]: Server running on ${server.info.uri}`)
}

init()
